1.2.2 - 08.14.19
================
* Add a script example to demonstrate data download, execution of the CLI, and validation of the outputs.
* Add support for environement variables available from one step to another. (diva_evaluation_cli.bin.private_src.implementation.utils.actev_cli_environment.*)

1.2.1 - 07.24.19
================
* Add baseline outputs on ActEV-Eval-CLI-Validation-Set3
* Update baseline outputs on ActEV-Eval-CLI-Validation-Set1 and ActEV-Eval-CLI-Validation-Set2
* Bug fixes in the validation-execution command


1.2.0 - 07.01.19
================
* Update validate-execution command with a `--score` flag https://gitlab.kitware.com/actev/diva_evaluation_cli/issues/11
* Update validate-system command, and add a `--strict` flag https://gitlab.kitware.com/actev/diva_evaluation_cli/issues/12
* Improved documentation for each command
* New _free disk_ metric in resources monitoring
* Bug fixes

1.1.9 - 05.23.19
================

* Bug fixes

1.1.8 - 05.15.19
================

* Bug fixes
* Less constraints on the container_output validation

1.1.7 - 04.09.19
================

* Bug fixes
* Add a new validation set: ActEV-Eval-CLI-Validation-Set2

1.1.6 - 04.01.19
================

* Improve get-system 'git': add 'recursive' option to download submodules

1.1.5 - 03.04.19
================

* Remove get-system subcommand: docker
* Rename get-system 'other' subcommand into 'archive'
* Improve get-system 'archive' and 'git' subcommands

1.1.4 - 02.22.19
================

* Bug fixes
* Add a new command: actev clear-logs

1.1.3 - 02.06.19
================

* Bug fixes

1.1.2 - 11.21.18
================

* Add a new data set: ActEV Validation Set1
* Bug fixes

1.1.1 - 11.20.18
================

* Add a new feature: resource monitoring
* Add some comments in the entry points
* Bug fixes

1.1 - 11.16.18
==============

* Add a new command: actev validate-execution
* Complete documentation
* Modify installation script: add requirements installation 

1.0.3 - 11.09.18
================

* Add a new command: actev status
* Add documentation about actev status
* Fix a bug in actev exec when nb_videos_per_chunk was missing
* Add actions before and after command: check video files validity in experiment-init

1.0.2 - 10.26.18
================

* Fix the installation script: support python virtual environments
* Modify the installation section of the README
* Add a new argument to pre/post/process-chunk: --system-cache-dir

1.0.1 - 10.24.18
================

* Add a new argument to reset-chunk: --system-cache-dir
* Modify names of the arguments composed of multiple words. Example: --chunk_id becomes --chunk-id

1.0 - 10.23.18
==============

* Release the abstract CLI: available on the master branch
* Release a baseline system CLI implementation: available on the baseline_system_master branch

