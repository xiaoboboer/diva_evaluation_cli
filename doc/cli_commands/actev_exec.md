# actev exec

## Description

Calls in sequence the following CLI commands:

	* experiment_init
	* for each chunk,
        * pre_process_chunk
        * process_chunk
        * post_process_chunk
    * merge_chunks
	* experiment_cleanup

This command requires the following parameters:

## Parameters

| Name      | Id         | Required | Definition                 |
|-----------|------------|---------|----------------------------|
| file-index         | f | True    | path to file index json file       |
| activity-index     | a | True    | path to activity index json file   |
| chunks             | c | True    | path to chunks json file           |
| nb-video-per-chunk | n | False   | number of videos in the chunk      |
| video-location     | v | True    | path to videos content             |   
| system-cache-dir   | s | True    | path to system cache directory     |
| config-file        | C | False   | path to config file                |
| output-file        | o | True    | path to merge chunks command result|
| chunk_result       | r | True    | path to chunks json file after merge chunks execution |


## Usage

Generic command:

```
actev exec -f <path to file.json> -a <path to activity.json> -c <path to chunks.json > -n <number of videos per chunk> -v <path to videos directory> -s <path to system cache directory> -C <path to config file.json> -o <path to output.json> -r <path to chunks result>
```

Example:

```
actev exec -f ~/file.json -a ~/activity.json -c ~/chunks.json -n 4 -v ~/videos/ -s ~/system_cache_dir/ -o ~/output.json -r ~/chunks_result.json
```

