"""Init experiment source code

This module is used to get videos that are not already processed by the system.

"""
import json
import sys
import os

def get_videos_to_transform_into_frames(file_json, output, frames_directory):
    """ Get videos from a file json file and save them in a new file

    Args:
        file_json (str): Path to file json
        output (str): Path to save unprocessed videos
        frames_directory (str): Path to directory containing frames of the videos
    
    """
    videos_id = []
    videos = json.load(open(file_json, "r"))
    with open(output, "w") as f:
        for video in videos:
      
            (exists, frames_path) = are_frames_exist(video, frames_directory)
            if not exists:
                f.write(video + ',' + frames_path + "\n")

def are_frames_exist(video, frames_directory):
    """ Check if videos are already transformed into frames
    
    Args:
        video (str): Path to a video file
        frames_directory (str): Path to directory containing frames of the videos

    Return:
        bool: indicating wether videos has been processed

    """
    video_name = os.path.splitext(video)[0]
    frames_path = os.path.join(frames_directory, video_name)
    return (os.path.exists(frames_path), frames_path)


if __name__ == '__main__':
    if len(sys.argv) == 4:
        get_videos_to_transform_into_frames(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        raise Exception("Input error.")
