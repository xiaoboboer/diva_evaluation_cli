#!/bin/bash

CHUNK_ID=$1

nvidia-docker start rc3d

nvidia-docker exec -it rc3d /bin/bash -c "python /diva/R-C3D/experiments/virat/main_wrapper.py --exp /diva/R-C3D/experiments/virat/experiment.yml --model_cfg /diva/R-C3D/experiments/virat/td_cnn_end2end.yml --skip_train --test-activity-index /diva/nist-json/activity-index.json --chunk_id=${CHUNK_ID}"
