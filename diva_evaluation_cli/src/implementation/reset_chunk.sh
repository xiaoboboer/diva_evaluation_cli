#!/bin/bash

CHUNK_ID=$1
SYSTEM_CACHE_DIR=$2

echo "Reseting ${1}"

OUTPUT=`echo "${CHUNK_ID}_sysfile.json"`
rm -f "${SYSTEM_CACHE_DIR}/${OUTPUT}"
