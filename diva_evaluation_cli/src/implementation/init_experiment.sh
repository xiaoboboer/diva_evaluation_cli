#!/bin/bash

cd "$(dirname "$0")"
echo 'begin init'
FILE_INDEX=$1
ACTIVITY_INDEX=$2
CHUNK=$3
VIDEO_LOCATION=$4
SYSTEM_CACHE_DIRECTORY=$5
echo 'file loaded.'
TMP_FILE="videos.tmp"

# Clean the experiment if rc3d container already exists
docker ps -a | grep rc3d 1> /dev/null
if [ $? -eq 0  ];then
  ./clean_up.sh
fi

FRAMES=`echo "${SYSTEM_CACHE_DIRECTORY}/frames"`

# Transform videos into frames
python3 get_videos.py $FILE_INDEX $TMP_FILE $FRAMES
for CONTENT in $(cat $TMP_FILE); do
  VIDEO=`echo $CONTENT | cut -d ',' -f1`
  FRAMES_PATH=`echo $CONTENT | cut -d ',' -f2`
  mkdir -p "${FRAMES_PATH}"
  echo "${VIDEO_LOCATION}/${VIDEO}"
  ffmpeg -i "${VIDEO_LOCATION}/${VIDEO}" "${FRAMES_PATH}/%05d.png"
done

rm $TMP_FILE

# Start the rc3d container
nvidia-docker  run -itd --name rc3d \
  --entrypoint bash \
  -v ${FRAMES}:/data/diva/frames \
  -v ${SYSTEM_CACHE_DIRECTORY}:/data/diva/system-cache \
  gitlab.kitware.com:4567/xiaoboboer/diva_evaluation_cli:v1

nvidia-docker exec rc3d mkdir -p /diva/nist-json
nvidia-docker cp ${FILE_INDEX} rc3d:/diva/nist-json/file-index.json
nvidia-docker cp ${ACTIVITY_INDEX} rc3d:/diva/nist-json/activity-index.json
nvidia-docker cp ${CHUNK} rc3d:/diva/nist-json/chunk.json

