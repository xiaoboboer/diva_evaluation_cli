"""
SETUP MODULE

Description
-----------
Generate the package for the CLI.

Warning: this file should not be modified.
"""
from setuptools import setup, find_packages
import diva_evaluation_cli

setup(name='diva_evaluation_cli',
      version=diva_evaluation_cli.__version__,
      package=find_packages(),
      include_package_data=True,
      entry_points=
      '''
      [console_scripts]
      actev=diva_evaluation_cli.bin.cli:main
      '''
      )

